ELEMENT.locale(ELEMENT.lang.trTR)

new Vue({
	el: '#app',
	data: function() {
		return { 
			product: {},
			productList: [],
			productListTemp: [],
			searchText: null,
			displayTable: false,
			displayTypeProduct: false,
			money: {
				decimal: '.',
				thousands: ',',
				prefix: '₺',
				precision: 2,
				masked: false
			}
		}
	},
	created(){
		this.$http.get('./asts/products.json').then(response => {
			this.productList 		= response.body;
			this.productListTemp 	= response.body;
		}, response => {
			this.$message({
				showClose: true,
				message: response.statusText,
				type: "error"
			});
		});
	},
	methods: {
		search(searchText){
			let searchResult = this.filterProducts(true);
			if(searchResult.length == 1){
				this.selectProduct(searchResult[0])
			}else if(searchResult.length == 0){
				this.$message({
					showClose: true,
					message: 'Ürün Bulunamadı!',
					type: "error"
				});				
			}
		},
		filterTable(){
			this.productList = this.filterProducts();
			if(this.productList.length > 0 && this.productList.length != this.productListTemp.length){
				this.displayTable = true;
			}else{
				this.displayTable = false;
			}
		},
		filterProducts() {
			productList = this.productListTemp;
			if (this.searchText && this.searchText.trim() != '') {
				productList = productList.filter((product) => {
					let productTemp = product.name + ' - ' + product.model;
					return (productTemp.toLowerCase().indexOf(this.searchText.toLowerCase()) > -1);
				})
			}
			return productList;
		},
		selectProduct(product){
			this.displayTypeProduct = true;
			product.price 			= parseFloat(product.price);
			product.quantity 		= parseInt(product.quantity);
			this.product = product;
		},
		updateProduct(){
			this.displayTypeProduct = false;
			//update Product Process
		}
	},

})
